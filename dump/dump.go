package dump

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
)

func AST(filepath string) error {
	fs := token.NewFileSet()
	
	code, err := parser.ParseFile(fs, filepath, nil, 0)
	if err != nil {
		return fmt.Errorf("failed to load Go file: %v", err)
	}

	ast.Print(fs, code)
	return nil
}
